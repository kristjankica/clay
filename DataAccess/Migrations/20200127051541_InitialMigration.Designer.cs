﻿// <auto-generated />
using System;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DataAccess.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200127051541_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Contract.Data.Door", b =>
                {
                    b.Property<string>("Id");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Make");

                    b.Property<string>("Model");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Doors");
                });

            modelBuilder.Entity("Contract.Data.DoorAccess", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("DoorId");

                    b.Property<bool>("Open");

                    b.Property<bool>("Success");

                    b.Property<int>("TagId");

                    b.HasKey("Id");

                    b.ToTable("DoorAccesses");
                });

            modelBuilder.Entity("Contract.Data.DoorTag", b =>
                {
                    b.Property<string>("DoorId");

                    b.Property<int>("TagId");

                    b.Property<DateTime>("CreatedAt");

                    b.HasKey("DoorId", "TagId");

                    b.HasIndex("TagId");

                    b.ToTable("DoorTags");
                });

            modelBuilder.Entity("Contract.Data.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<byte[]>("Hash");

                    b.Property<byte[]>("Salt");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("Contract.Data.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Admin");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<byte[]>("PasswordHash");

                    b.Property<byte[]>("PasswordSalt");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasFilter("[Email] IS NOT NULL");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Contract.Data.Door", b =>
                {
                    b.HasOne("Contract.Data.User", "User")
                        .WithMany("Doors")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Contract.Data.DoorTag", b =>
                {
                    b.HasOne("Contract.Data.Door", "Door")
                        .WithMany("DoorTags")
                        .HasForeignKey("DoorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Contract.Data.Tag", "Tag")
                        .WithMany("DoorTags")
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Contract.Data.Tag", b =>
                {
                    b.HasOne("Contract.Data.User", "User")
                        .WithMany("Tags")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
