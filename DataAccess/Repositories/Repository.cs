﻿
using Contract.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal ApplicationDbContext context;
        internal DbSet<TEntity> dbSet;
        public Repository(ApplicationDbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }
        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "",
            int pageIndex = 1,
            int pageSize = 10)
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties
                 .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (pageIndex > 0)
            {
                query = query.OrderByDescending(x => x);
                query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            if (orderBy != null)
            {
                return orderBy(query);
            }
            else
            {
                return query;
            }
        }
        public virtual async Task<TEntity> GetAsync(params object[] id)
        {
            return await dbSet.FindAsync(id);
        }

        public virtual async Task<bool> ExistsAsync(object id)
        {
            return await GetAsync(id) != null;
        }
        public virtual async Task AddAsync(TEntity entity)
        {
            await context.AddAsync(entity);
            await SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(int id)
        {
            TEntity entityToDelete = await GetAsync(id);
            if (entityToDelete != null)
            {
                await DeleteAsync(entityToDelete);
            }
        }
        public virtual async Task DeleteAsync(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            await SaveChangesAsync();
        }
        public virtual async Task UpdateAsync(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
            await SaveChangesAsync();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return dbSet;
        }
        private async Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }

        public virtual TEntity Copy(TEntity source)
        {
            return (TEntity)context.Entry(source).CurrentValues.ToObject();
        }

        public virtual async Task UpdateRangeAsync(IEnumerable<TEntity> entities)
        {
            dbSet.UpdateRange(entities);
            await SaveChangesAsync();
        }

        public virtual async Task DeleteRangeAsync(IEnumerable<TEntity> entities)
        {
            dbSet.RemoveRange(entities);
            await SaveChangesAsync();
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            dbSet.AddRange(entities);
            await SaveChangesAsync();
        }

        public virtual async Task UpdateSetAsync(IEnumerable<TEntity> add, IEnumerable<TEntity> delete, IEnumerable<TEntity> update)
        {
            if (add != null)
            {
                dbSet.AddRange(add);
            }
            if (delete != null)
            {
                dbSet.RemoveRange(delete);
            }
            if (update != null)
            {
                dbSet.UpdateRange(update);
            }
            await SaveChangesAsync();
        }
    }
}
