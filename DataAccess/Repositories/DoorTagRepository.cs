﻿using Contract.Data;
using Contract.Repositories;

namespace DataAccess.Repositories
{
    public class DoorTagRepository : Repository<DoorTag>, IDoorTagRepository
    {
        public DoorTagRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
