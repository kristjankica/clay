﻿using Contract.Data;
using Contract.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {

        }

        public Task<List<Door>> GetAllDoorsByUserId(int id)
        {
            return context.Doors
                .Where(d => d.UserId == id)
                .ToListAsync();
        }
        public Task<User> GetByEmailAsync(string email)
        {
            return dbSet
                .FirstOrDefaultAsync(u => u.Email.Equals(email));
        }
        public override async Task DeleteAsync(User entityToDelete)
        {
            // workaround to multiple cascading paths to DoorTag
            var doorsToDelete = await GetAllDoorsByUserId(entityToDelete.Id);
            context.Doors
                .RemoveRange(doorsToDelete);
            await context.SaveChangesAsync();

            await base.DeleteAsync(entityToDelete);

        }
    }
}
