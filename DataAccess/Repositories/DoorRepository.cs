﻿using Contract.Data;
using Contract.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class DoorRepository : Repository<Door>, IDoorRepository
    {
        public DoorRepository(ApplicationDbContext context) : base(context)
        {

        }

        public Task<List<Tag>> GetTagsForDoorAsync(string id)
        {
            return context.DoorTags
                .Include(dt => dt.Tag)
                .Where(dt => dt.DoorId.Equals(id))
                .Select(dt => dt.Tag)
                .ToListAsync();
        }
    }
}
