﻿
using Contract.Data;
using Contract.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class DoorAccessRepository : Repository<DoorAccess>, IDoorAccessRepository
    {
        public DoorAccessRepository(ApplicationDbContext context) : base(context)
        {

        }

        public Task<List<DoorAccess>> GetAllByDoorIdAsync(string doorId)
        {
            return
                dbSet.Where(d => d.DoorId.Equals(doorId))
                .ToListAsync();
        }
    }
}
