﻿using Contract.Data;
using Contract.Repositories;


namespace DataAccess.Repositories
{
    public class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
