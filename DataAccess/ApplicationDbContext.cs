﻿using Contract.Data;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Door> Doors { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<DoorTag> DoorTags { get; set; }
        public DbSet<DoorAccess> DoorAccesses { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DoorTag>()
                .HasKey(o => new { o.DoorId, o.TagId });

            modelBuilder.Entity<User>()
                .HasIndex(o => o.Email)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Doors)
                .WithOne(d => d.User)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);// multiple paths to DoorTag

            modelBuilder.Entity<Door>()
                .HasMany(x => x.DoorTags)
                .WithOne(y => y.Door)
                .HasForeignKey(y => y.DoorId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Tag>()
                .HasMany(x => x.DoorTags)
                .WithOne(y => y.Tag)
                .HasForeignKey(y => y.TagId)
                .OnDelete(DeleteBehavior.Cascade);


        }
    }
}
