﻿using System.ComponentModel.DataAnnotations;

namespace Contract.Models
{
    public class RequiredNotEmptyAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is string)
                return !string.IsNullOrWhiteSpace((string)value);
            if (value is int)
                return ((int)value) > 0;

            return base.IsValid(value);
        }
    }
}
