﻿using AutoMapper;
using Contract.Data;
using Contract.Models.DoorsTags;

namespace Contract.Models.Mapper
{
    public class DoorTagMapper : Profile
    {
        public DoorTagMapper()
        {
            CreateMap<CreateDoorTagModel, DoorTag>();
        }
    }
}
