﻿using AutoMapper;
using Contract.Data;
using Contract.Models.Users;


namespace Contract.Helpers.Mapper
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
            CreateMap<UpdateUserModel, User>();
            CreateMap<User, string>()
                .ConvertUsing(u => u == null ? "" : $"{u.FirstName} {u.LastName}");
        }
    }
}
