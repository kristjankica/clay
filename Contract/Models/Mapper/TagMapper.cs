﻿using AutoMapper;
using Contract.Data;
using Contract.Models.Tags;

namespace Contract.Models.Mapper
{
    public class TagMapper : Profile
    {
        public TagMapper()
        {
            CreateMap<Tag, TagModel>();
        }
    }
}
