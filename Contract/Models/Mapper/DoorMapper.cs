﻿using AutoMapper;
using Contract.Data;
using Contract.Models.Doors;

namespace Contract.Models.Mapper
{
    public class DoorMapper : Profile
    {
        public DoorMapper()
        {
            CreateMap<CreateDoorModel, Door>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Mac));
            CreateMap<Door, DoorModel>();
        }
    }
}
