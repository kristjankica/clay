namespace Contract.Models.Users
{
    public class AuthenticateModel
    {
        [RequiredNotEmpty]
        public string Email { get; set; }

        [RequiredNotEmpty]
        public string Password { get; set; }
    }
}