

namespace Contract.Models.Users
{
    public class RegisterModel
    {
        [RequiredNotEmpty]
        public string FirstName { get; set; }

        [RequiredNotEmpty]
        public string LastName { get; set; }

        [RequiredNotEmpty]
        public string Email { get; set; }

        [RequiredNotEmpty]
        public string Password { get; set; }
    }
}