﻿namespace Contract.Models.Doors
{
    public class CreateDoorModel
    {
        [RequiredNotEmpty]
        public string Mac { get; set; }
        [RequiredNotEmpty]
        public string Make { get; set; }
        [RequiredNotEmpty]
        public string Model { get; set; }
    }
}
