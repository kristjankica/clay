﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Models.Doors
{
    public class DoorModel
    {
        public string Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int UserId { get; set; }
    }
}
