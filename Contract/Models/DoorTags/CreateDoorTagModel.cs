﻿namespace Contract.Models.DoorsTags
{
    public class CreateDoorTagModel
    {
        [RequiredNotEmpty]
        public int TagId { get; set; }
    }
}
