﻿namespace Contract.Models.Tags
{
    public class DoorActionModel
    {
        [RequiredNotEmpty]
        public string Tag { get; set; }
        public bool Open { get; set; }
    }
}
