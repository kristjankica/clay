﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Models.Tags
{
    public class TagModel
    {
        public int Id { get; set; }
        public string Tag { get; set; }
        public int UserId { get; set; }
    }
}
