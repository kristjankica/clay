﻿using Contract.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contract.Repositories
{
    public interface IDoorRepository : IRepository<Door>
    {
        Task<List<Tag>> GetTagsForDoorAsync(string id);
    }
}
