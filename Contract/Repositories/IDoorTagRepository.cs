﻿using Contract.Data;

namespace Contract.Repositories
{
    public interface IDoorTagRepository : IRepository<DoorTag>
    {
    }
}
