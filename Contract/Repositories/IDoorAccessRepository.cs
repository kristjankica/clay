﻿using Contract.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contract.Repositories
{
    public interface IDoorAccessRepository : IRepository<DoorAccess>
    {
        Task<List<DoorAccess>> GetAllByDoorIdAsync(string doorId);
    }
}
