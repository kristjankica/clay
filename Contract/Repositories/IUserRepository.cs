﻿using Contract.Data;
using System.Threading.Tasks;

namespace Contract.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByEmailAsync(string email);
    }
}
