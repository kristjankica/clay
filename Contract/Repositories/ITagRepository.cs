﻿using Contract.Data;


namespace Contract.Repositories
{
    public interface ITagRepository : IRepository<Tag>
    {
    }
}
