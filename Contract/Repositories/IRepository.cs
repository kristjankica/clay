﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contract.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(params object[] id);
        Task<bool> ExistsAsync(object id);
        Task AddAsync(TEntity entity);
        Task DeleteAsync(int id);
        Task DeleteAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        TEntity Copy(TEntity source);
        Task UpdateRangeAsync(IEnumerable<TEntity> entities);
        Task DeleteRangeAsync(IEnumerable<TEntity> entities);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        Task UpdateSetAsync(IEnumerable<TEntity> add, IEnumerable<TEntity> delete, IEnumerable<TEntity> update);
    }
}
