﻿using System.Collections.Generic;


namespace Contract.Data
{
    public class User : BaseEntity
    {
        public User()
        {
            Tags = new List<Tag>();
            Doors = new List<Door>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool Admin { get; set; } = false;
        public List<Tag> Tags { get; set; }
        public List<Door> Doors { get; set; }

    }
}
