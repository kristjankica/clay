﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contract.Data
{
    public class Tag : BaseEntity
    {
        public Tag()
        {
            DoorTags = new List<DoorTag>();
        }
        [Key]
        public int Id { get; set; }
        public byte[] Hash { get; set; }
        public byte[] Salt { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public List<DoorTag> DoorTags { get; set; }
    }
}
