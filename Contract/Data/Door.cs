﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Contract.Data
{
    public class Door : BaseEntity
    {
        public Door()
        {
            DoorTags = new List<DoorTag>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public List<DoorTag> DoorTags { get; set; }
    }
}
