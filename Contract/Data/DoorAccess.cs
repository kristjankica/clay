﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contract.Data
{
    public class DoorAccess : BaseEntity
    {
        public int Id { get; set; }
        public string DoorId { get; set; }
        public int TagId { get; set; }

        public bool Open { get; set; }

        public bool Success { get; set; }
        [NotMapped]
        public string Action => Open ? "Open" : "Close";
    }
}
