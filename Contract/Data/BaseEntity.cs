﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Data
{
    public class BaseEntity
    {
        private DateTime? createdAt = null;
        public DateTime CreatedAt
        {
            get
            {
                return createdAt.HasValue
                   ? createdAt.Value
                   : DateTime.UtcNow;
            }

            set { createdAt = value; }
        }
    }
}
