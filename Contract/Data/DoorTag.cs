﻿using System.ComponentModel.DataAnnotations;

namespace Contract.Data
{
    public class DoorTag : BaseEntity
    {
        public int TagId { get; set; }
        public Tag Tag { get; set; }
        public string DoorId { get; set; }
        public Door Door { get; set; }
    }
}
