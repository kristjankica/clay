﻿namespace Contract.Services
{
    public interface IJwtTokenService
    {
        string GetToken(string userName);
        string GetUserName(string token);
    }
}
