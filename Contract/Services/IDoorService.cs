﻿
namespace Contract.Services
{
    public interface IDoorService
    {
        bool Act(string mac, string make, string model, bool open);
    }
}
