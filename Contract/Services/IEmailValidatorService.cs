﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Services
{
    public interface IEmailValidatorService
    {
        bool Validate(string email);
    }
}
