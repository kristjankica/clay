﻿using Controllers;
using Xunit;

namespace Tests
{
    public class BaseControllerTests : ControllerTests
    {
        [Fact]
        public void MyId_ReturnsId_WhenIdSet()
        {
            int id = 1;
            BaseController controller = new BaseController(null, null)
            {
                ControllerContext = GetControllerContext(id)
            };
            var result = controller.CallNonPublicProperty<int?>("MyId");
            Assert.Equal(id, result);
        }


        [Fact]
        public void MyId_ReturnsNull_WhenIdNotSet()
        {
            BaseController controller = new BaseController(null, null)
            {
                ControllerContext = GetControllerContext()
            };
            var result = controller.CallNonPublicProperty<int?>("MyId");
            Assert.Null(result);
        }

        [Fact]
        public void BadRequest_ReturnsBadRequestObjectResult()
        {
            var controller = new BaseController(null, null);

            var result = controller.BadRequest(1);
            Assert.NotNull(result);
            Assert.Equal(1, result.Value);
            result = controller.BadRequest("");
            Assert.NotNull(result);
            Assert.Equal("", result.Value);
        }
    }
}
