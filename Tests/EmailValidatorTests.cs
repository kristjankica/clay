using Services;
using System;
using Xunit;

namespace Tests
{
    public class EmailValidatorTests
    {
        private readonly EmailValidatorService _emailValidator = new EmailValidatorService();
        public EmailValidatorTests()
        {

        }

        [Fact]
        public void Validate_True_ValidEmail()
        {
            Assert.True(_emailValidator.Validate("kristjan.kica@fti.edu.al"));
        }

        [Fact]
        public void Validate_False_MissingAtSymbol()
        {
            Assert.False(_emailValidator.Validate("kristjan.kicafti.edu.al"));
        }

        [Fact]
        public void Validate_False_Empty()
        {
            Assert.False(_emailValidator.Validate(""));
        }

        [Fact]
        public void Validate_False_MultpleAtSymbols()
        {
            Assert.False(_emailValidator.Validate("kristjan.kica@@fti.edu.al"));
        }

        [Fact]
        public void Validate_ArgumentNullException_Null()
        {
            Assert.Throws<ArgumentNullException>(() => _emailValidator.Validate(null));
        }

    }
}
