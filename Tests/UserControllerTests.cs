﻿using Contract.Data;
using Contract.Models.Users;
using Contract.Repositories;
using Contract.Services;
using Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class UserControllerTests : ControllerTests
    {

        #region register
        [Fact]
        public async void RegisterAsync_ReturnsOkObjectResult_WhenValidData_Async()
        {
            int id = 1;
            string validEmail = "validEmail";
            string validPassword = "validPassword";
            string validFirstName = "validFirstName";
            string validLastName = "validLastName";
            var model = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = validLastName
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetByEmailAsync(validEmail))
                .ReturnsAsync((User)null);

            Mock<IEmailValidatorService> mockEmailValidator = new Mock<IEmailValidatorService>();
            mockEmailValidator
                .Setup(m => m.Validate(validEmail))
                .Returns(true);

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.CreatePasswordHash(validPassword, out storedHash, out storedSalt));

            var controller = GetMockedController(null, mockUserRepository, null, mockEmailValidator, mockHasherService);

            // act
            var result = await controller.RegisterAsync(model);

            //assert
            Assert.IsType<OkObjectResult>(result);
        }


        [Fact]
        public async void RegisterAsync_ReturnsBadRequestObjectResult_WhenInvalidData_Async()
        {
            string invalidEmail = "invalidEmail";
            string validEmail = "validEmail";
            string validPassword = "validPassword";
            string validFirstName = "validFirstName";
            string validLastName = "validLastName";
            var invalidEmailModel = new RegisterModel()
            {
                Email = invalidEmail,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = validLastName
            };
            var nullEmailModel = new RegisterModel()
            {
                Email = null,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = validLastName
            };

            var emptyEmailModel = new RegisterModel()
            {
                Email = string.Empty,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = validLastName
            };

            var emptyFirstNameModel = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = string.Empty,
                LastName = validLastName
            };

            var nullFirstNameModel = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = null,
                LastName = validLastName
            };

            var emptyLastNameModel = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = string.Empty
            };

            var nullLastNameModel = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = null
            };

            var emptyPasswordModel = new RegisterModel()
            {
                Email = validEmail,
                Password = string.Empty,
                FirstName = validFirstName,
                LastName = validLastName
            };

            var nullPasswordModel = new RegisterModel()
            {
                Email = validEmail,
                Password = null,
                FirstName = validFirstName,
                LastName = validLastName
            };

            Mock<IEmailValidatorService> mockEmailValidator = new Mock<IEmailValidatorService>();
            mockEmailValidator
                .Setup(m => m.Validate(invalidEmail))
                .Returns(false);
            mockEmailValidator
                .Setup(m => m.Validate(validEmail))
                .Returns(true);
            var controller = GetMockedController(mockEmailValidator: mockEmailValidator);

            controller.BindModel(invalidEmailModel);
            var result = await controller.RegisterAsync(invalidEmailModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullEmailModel);
            result = await controller.RegisterAsync(nullEmailModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(emptyEmailModel);
            result = await controller.RegisterAsync(emptyEmailModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(emptyFirstNameModel);
            result = await controller.RegisterAsync(emptyFirstNameModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullFirstNameModel);
            result = await controller.RegisterAsync(nullFirstNameModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(emptyLastNameModel);
            result = await controller.RegisterAsync(emptyLastNameModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullLastNameModel);
            result = await controller.RegisterAsync(nullLastNameModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(emptyPasswordModel);
            result = await controller.RegisterAsync(emptyPasswordModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullPasswordModel);
            result = await controller.RegisterAsync(nullPasswordModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void RegisterAsync_ReturnsBadRequestObjectResult_WhenEmailInUse_Async()
        {
            int id = 1;
            string validEmail = "validEmail";
            string validPassword = "validPassword";
            string validFirstName = "validFirstName";
            string validLastName = "validLastName";
            var model = new RegisterModel()
            {
                Email = validEmail,
                Password = validPassword,
                FirstName = validFirstName,
                LastName = validLastName
            };

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetByEmailAsync(validEmail))
                .Returns(Task.FromResult(new User { Email = validEmail, Id = id }));

            Mock<IEmailValidatorService> mockEmailValidator = new Mock<IEmailValidatorService>();
            mockEmailValidator
                .Setup(m => m.Validate(validEmail))
                .Returns(true);

            var controller = GetMockedController(null, mockUserRepository, mockEmailValidator: mockEmailValidator);

            var result = await controller.RegisterAsync(model);

            Assert.IsType<BadRequestObjectResult>(result);
        }
        #endregion

        #region authenticate

        [Fact]
        public async void AuthenticateAsync_ReturnsToken_WhenValidData_Async()
        {
            string validEmail = "validEmail";
            string validPassword = "validPassword";
            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            int id = 1;
            var model = new AuthenticateModel()
            {
                Email = validEmail,
                Password = validPassword
            };
            string token = "expectedToken";

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetByEmailAsync(validEmail))
                .Returns(Task.FromResult(new User
                {
                    Id = id,
                    Email = validEmail,
                    PasswordSalt = storedSalt,
                    PasswordHash = storedHash,
                }));


            var mockJwtTokenService = new Mock<IJwtTokenService>();
            mockJwtTokenService
                .Setup(m => m.GetToken(id.ToString()))
                .Returns(token);

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(validPassword, storedHash, storedSalt))
                .Returns(true);

            var controller = GetMockedController(null, mockUserRepository, mockJwtTokenService: mockJwtTokenService, mockHasherService: mockHasherService);

            var result = await controller.AuthenticateAsync(model);
            Assert.IsType<OkObjectResult>(result);
            Assert.Equal(token, (result as OkObjectResult).Value.CallProperty<string>("Token"));
        }

        [Fact]
        public async void AuthenticateAsync_ReturnsNotFoundObjectResult_WhenEmailDoesNotExist_Async()
        {
            string validEmail = "validEmail";
            string invalidEmail = "invalidEmail";
            string validPassword = "validPassword";
            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            int id = 1;

            var invalidEmailModel = new AuthenticateModel()
            {
                Email = invalidEmail,
                Password = validPassword
            };


            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetByEmailAsync(validEmail))
                .Returns(Task.FromResult(new User
                {
                    Id = id,
                    Email = validEmail,
                    PasswordSalt = storedSalt,
                    PasswordHash = storedHash,
                }));

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(validPassword, storedHash, storedSalt))
                .Returns(true);

            var controller = GetMockedController(null, mockUserRepository, mockHasherService: mockHasherService);

            var result = await controller.AuthenticateAsync(invalidEmailModel);
            Assert.IsType<NotFoundObjectResult>(result);
        }


        [Fact]
        public async void AuthenticateAsync_ReturnsUnauthorizedObjectResult_WhenInValidData_Async()
        {
            string validEmail = "validEmail";
            string invalidPassword = "invalidPassword";
            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            int id = 1;

            var invalidPasswordModel = new AuthenticateModel()
            {
                Email = validEmail,
                Password = invalidPassword
            };

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetByEmailAsync(validEmail))
                .Returns(Task.FromResult(new User
                {
                    Id = id,
                    Email = validEmail,
                    PasswordSalt = storedSalt,
                    PasswordHash = storedHash,
                }));

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(invalidPassword, storedHash, storedSalt))
                .Returns(false);

            var controller = GetMockedController(null, mockUserRepository, mockHasherService: mockHasherService);

            var result = await controller.AuthenticateAsync(invalidPasswordModel);
            Assert.IsType<UnauthorizedObjectResult>(result);
        }
        #endregion

        #region getAll
        [Fact]
        public async Task GetAll_ReturnsOkObjectResultAsync()
        {
            IEnumerable<User> users = new List<User>()
            {
                new User(){
                    Id = 1,

                    Email = "email1",
                    PasswordSalt = new byte[64],
                    PasswordHash = new byte[128],
                    FirstName="user1Name",
                    LastName="user1LastName"
                },
                new User(){
                    Id = 1,

                    Email = "email2",
                    PasswordSalt = new byte[64],
                    PasswordHash = new byte[128],
                    FirstName="user2Name",
                    LastName="user2LastName"
                }
            };
            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAllAsync())
                .Returns(Task.FromResult(users));

            var controller = GetMockedController(null, mockUserRepository);

            var result = await controller.GetAllAsync();
            Assert.IsType<OkObjectResult>(result);
            var returnedUsers = (result as OkObjectResult).Value;
            Assert.IsAssignableFrom<IEnumerable<UserModel>>(returnedUsers);
            var actual = (returnedUsers as IEnumerable<UserModel>).ToList();
            var expected = users as List<User>;
            Assert.Equal(actual.Count, expected.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                AssertCorrectUserModel(expected[i], actual[i]);
            }
        }

        #endregion


        #region get
        [Fact]
        public async void GetAsync_ReturnsOkObjectResult_WhenRecordOwnerAsync()
        {
            var requestedId = 1;
            var user = new User()
            {
                Id = requestedId,

                Email = "email",
                PasswordSalt = new byte[64],
                PasswordHash = new byte[128],
                FirstName = "userName",
                LastName = "userLastName"
            };

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(requestedId))
                .Returns(Task.FromResult(user));

            var controller = GetMockedController(requestedId, mockUserRepository);

            var result = await controller.GetByIdAsync(requestedId);

            Assert.IsType<OkObjectResult>(result);
            var model = (result as OkObjectResult).Value as UserModel;
            AssertCorrectUserModel(user, model);
        }

        #endregion


        #region updateUser

        [Fact]
        public async void UpdateUserAsync_ReturnsOkObjectResult_WhenValidData()
        {
            var id = 1;
            var user = new User()
            {
                Id = id,
                Email = "email",
                PasswordSalt = new byte[64],
                PasswordHash = new byte[128],
                FirstName = "userName",
                LastName = "userLastName"
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var updatedFirstName = "updatedFirstName";
            var updatedLastName = "updatedLastName";
            var updatedPassword = "updatedPassword";
            var updatedPasswordSalt = new byte[64];
            var updatedPasswordHash = new byte[128];
            var model = new UpdateUserModel()
            {

                FirstName = updatedFirstName,
                LastName = updatedLastName,
                Password = updatedPassword
            };
            var nullEmailModel = new UpdateUserModel()
            {
                FirstName = updatedFirstName,
                LastName = updatedLastName,
                Password = updatedPassword
            };
            var nullFirstNameModel = new UpdateUserModel()
            {
                FirstName = null,
                LastName = updatedLastName,
                Password = updatedPassword
            };
            var emptyLastNameModel = new UpdateUserModel()
            {
                FirstName = updatedFirstName,
                LastName = string.Empty,
                Password = updatedPassword
            };
            var nullPasswordModel = new UpdateUserModel()
            {
                FirstName = updatedFirstName,
                LastName = updatedLastName,
                Password = null
            };
            var emptyPasswordModel = new UpdateUserModel()
            {
                FirstName = updatedFirstName,
                LastName = updatedLastName,
                Password = null
            };

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .Returns(Task.FromResult(user));

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.CreatePasswordHash(updatedPassword, out storedHash, out storedSalt));

            var controller = GetMockedController(id, mockUserRepository, mockHasherService: mockHasherService);

            var result = await controller.UpdateAsync(id, model);
            Assert.IsType<OkObjectResult>(result);
            result = await controller.UpdateAsync(id, nullEmailModel);
            Assert.IsType<OkObjectResult>(result);
            result = await controller.UpdateAsync(id, nullFirstNameModel);
            Assert.IsType<OkObjectResult>(result);
            result = await controller.UpdateAsync(id, emptyLastNameModel);
            Assert.IsType<OkObjectResult>(result);
            result = await controller.UpdateAsync(id, nullPasswordModel);
            Assert.IsType<OkObjectResult>(result);
            result = await controller.UpdateAsync(id, emptyPasswordModel);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void UpdateUserAsync_ReturnsUnauthorizedResult_WhenUserTriesToUpdateAnotherUser()
        {
            var requestedId = 1;
            var userId = 2;
            var nonExistentUserId = 3;
            var user = new User()
            {
                Id = requestedId,

                Email = "email",
                PasswordSalt = new byte[64],
                PasswordHash = new byte[128],
                FirstName = "userName",
                LastName = "userLastName"
            };

            var updatedFirstName = "updatedFirstName";
            var updatedLastName = "updatedLastName";
            var updatedPassword = "updatedPassword";
            var model = new UpdateUserModel()
            {

                FirstName = updatedFirstName,
                LastName = updatedLastName,
                Password = updatedPassword
            };

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(userId))
                .Returns(Task.FromResult(new User { Id = userId }));
            mockUserRepository
                .Setup(m => m.GetAsync(requestedId))
                .Returns(Task.FromResult(user));
            mockUserRepository
                .Setup(m => m.GetAsync(nonExistentUserId))
                .Returns(Task.FromResult((User)null));

            var controller = GetMockedController(userId, mockUserRepository);

            var result = await controller.UpdateAsync(requestedId, model);
            Assert.IsType<UnauthorizedResult>(result);

            result = await controller.UpdateAsync(nonExistentUserId, model);
            Assert.IsType<UnauthorizedResult>(result);
        }



        #endregion

        #region delete
        [Fact]

        public async void DeleteAsync_ReturnsOkObjectResult_WhenValidData()
        {
            var id = 1;

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .Returns(Task.FromResult(new User() { Id = id }));

            var controller = GetMockedController(id, mockUserRepository);

            var result = await controller.DeleteAsync(id);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void DeleteAsync_ReturnsUnauthorizedObjectResult_WhenUserDoesNotExist()
        {
            var requestedId = 1;
            var id = 2;
            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.DeleteAsync(requestedId))
                .Returns(Task.CompletedTask);
            mockUserRepository
                .Setup(m => m.GetAsync(requestedId))
                .Returns(Task.FromResult((User)null));
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .Returns(Task.FromResult(new User { Id = id, }));

            var controller = GetMockedController(id, mockUserRepository);

            var result = await controller.DeleteAsync(requestedId);
            Assert.IsType<UnauthorizedObjectResult>(result);
        }

        #endregion
        private void AssertCorrectUserModel(User user, UserModel model)
        {
            Assert.Equal(user.Email, model.Email);
            Assert.Equal(user.FirstName, model.FirstName);
            Assert.Equal(user.Id, model.Id);
            Assert.Equal(user.LastName, model.LastName);
        }

        private UserController GetMockedController(int? userId = null,
            Mock<IUserRepository> mockUserRepository = null,
            Mock<IJwtTokenService> mockJwtTokenService = null,
            Mock<IEmailValidatorService> mockEmailValidator = null,
            Mock<IHasherService> mockHasherService = null)
        {
            return new UserController(
                (mockUserRepository ?? new Mock<IUserRepository>()).Object,
                _mapper,
                (mockJwtTokenService ?? new Mock<IJwtTokenService>()).Object,
                (mockEmailValidator ?? new Mock<IEmailValidatorService>()).Object,
                (mockHasherService ?? new Mock<IHasherService>()).Object, null)
            {
                ControllerContext = GetControllerContext(userId),
            };
        }
    }
}
