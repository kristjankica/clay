﻿using Contract.Data;
using Contract.Models.Tags;
using Contract.Models.Users;
using Contract.Repositories;
using Contract.Services;
using Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class TagControllerTests : ControllerTests
    {

        #region create
        [Fact]
        public async void CreateAsync_ReturnsOkObjectResult_Async()
        {
            int id = 1;

            var storedHash = new byte[64];
            var storedSalt = new byte[128];

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.CreatePasswordHash(It.IsAny<string>(), out storedHash, out storedSalt));

            var controller = GetMockedController(id, mockHasherService: mockHasherService);

            // act
            var result = await controller.CreateAsync();

            //assert
            Assert.IsType<OkObjectResult>(result);
            var tagModel = (result as OkObjectResult).Value as TagModel;
            Assert.False(string.IsNullOrEmpty(tagModel.Tag));
            Assert.Equal(id, tagModel.UserId);
        }


        [Fact]
        public async void CreateAsync_ReturnsBadRequestObjectResult_WhenException_Async()
        {
            int id = 1;

            var storedHash = new byte[64];
            var storedSalt = new byte[128];

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.CreatePasswordHash(It.IsAny<string>(), out storedHash, out storedSalt));
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                .Setup(m => m.AddAsync(It.IsAny<Tag>()))
                .ThrowsAsync(new Exception("Mocked Exception"));

            var controller = GetMockedController(id, mockHasherService: mockHasherService, mockTagRepository: mockTagRepository);

            // act
            var result = await controller.CreateAsync();

            //assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
        #endregion

        #region delete
        [Fact]
        public async void DeleteAsync_ReturnsOkObjectResult_WhenValidData()
        {
            var id = 1;
            var userid = 3;

            Mock<IUserRepository> mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync(new User() { Id = userid });

            Mock<ITagRepository> mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync(new Tag() { Id = id, UserId = userid });

            var controller = GetMockedController(userid, mockUserRepository, mockTagRepository);

            var result = await controller.DeleteAsync(id);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void DeleteAsync_ReturnsNotfoundObjectResult_WhenTagDoesNotExist()
        {
            var id = 1;
            var userid = 3;

            Mock<ITagRepository> mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync((Tag)null);

            var controller = GetMockedController(userid, mockTagRepository: mockTagRepository);

            var result = await controller.DeleteAsync(id);
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void DeleteAsync_ReturnsNotAuthorisedObjectResult_WhenTagBelongsToAnotherUser()
        {
            var id = 1;
            var userid = 3;
            var ownerId = 2;

            Mock<ITagRepository> mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync(new Tag { Id = id, UserId = ownerId });

            var controller = GetMockedController(userid, mockTagRepository: mockTagRepository);

            var result = await controller.DeleteAsync(id);
            Assert.IsType<UnauthorizedObjectResult>(result);
        }

        #endregion

        private TagController GetMockedController(int? userId = null,
            Mock<IUserRepository> mockUserRepository = null,
            Mock<ITagRepository> mockTagRepository = null,
            Mock<IHasherService> mockHasherService = null)
        {
            return new TagController(
                (mockUserRepository ?? new Mock<IUserRepository>()).Object,
                (mockTagRepository ?? new Mock<ITagRepository>()).Object,
                _mapper,
                (mockHasherService ?? new Mock<IHasherService>()).Object,
                null)
            {
                ControllerContext = GetControllerContext(userId),
            };
        }
    }
}
