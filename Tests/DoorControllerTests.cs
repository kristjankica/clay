﻿using Contract.Data;
using Contract.Models.Doors;
using Contract.Models.DoorsTags;
using Contract.Models.Tags;
using Contract.Repositories;
using Contract.Services;
using Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class DoorControllerTests : ControllerTests
    {

        #region create
        [Fact]
        public async void CreateAsync_ReturnsOkObjectResult_Async()
        {
            int id = 1;
            string validMake = "validMake";
            string validModel = "valdModel";
            string validMac = "validMac";

            var model = new CreateDoorModel()
            {
                Mac = validMac,
                Make = validMake,
                Model = validModel
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.ExistsAsync(model.Mac))
                .ReturnsAsync(false);
            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository);

            controller.BindModel(model);
            // act
            var result = await controller.CreateAsync(model);

            //assert
            Assert.IsType<OkObjectResult>(result);
        }


        [Fact]
        public async void CreateAsync_ReturnsBadRequestObjectResult_WhenInvalidData_Async()
        {
            int id = 1;
            string validMake = "validMake";
            string validModel = "valdModel";
            string validMac = "validMac";
            var nullMac = new CreateDoorModel()
            {
                Mac = null,
                Make = validMake,
                Model = validModel
            };
            var emptyMac = new CreateDoorModel()
            {
                Mac = string.Empty,
                Make = validMake,
                Model = validModel
            };

            var nullMake = new CreateDoorModel()
            {
                Mac = validMac,
                Make = null,
                Model = validModel
            };

            var emptyModel = new CreateDoorModel()
            {
                Mac = validMac,
                Make = validMake,
                Model = string.Empty
            };

            var nullModelEmptyMake = new CreateDoorModel
            {

                Mac = validMac,
                Make = string.Empty,
                Model = null
            };
            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.ExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(false);
            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository);


            controller.BindModel(nullMac);
            var result = await controller.CreateAsync(nullMac);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullMac);
            result = await controller.CreateAsync(emptyMac);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullMac);
            result = await controller.CreateAsync(nullMake);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullMac);
            result = await controller.CreateAsync(emptyModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(nullMac);
            result = await controller.CreateAsync(nullModelEmptyMake);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void CreateAsync_ReturnsBadRequestObjectResult_WhenDoorExists()
        {
            int id = 1;
            string validMake = "validMake";
            string validModel = "valdModel";
            string validMac = "validMac";

            var model = new CreateDoorModel()
            {
                Mac = validMac,
                Make = validMake,
                Model = validModel
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.ExistsAsync(It.IsAny<string>()))
                .ReturnsAsync(true);
            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository);


            controller.BindModel(model);
            var result = await controller.CreateAsync(model);
            Assert.IsType<BadRequestObjectResult>(result);
        }
        #endregion

        #region addTag
        [Fact]
        public async void AddTagAsync_ReturnsOkObjectResult_WhenValidData()
        {
            int id = 1;
            int validTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = validTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door
                {
                    UserId = id,
                    Id = mac
                });
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync(new Tag { Id = model.TagId, UserId = id });

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync((DoorTag)null);


            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsNotFoundObjectResult_WhenDoorDoesNotExist()
        {
            int id = 1;
            int validTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = validTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync((Door)null);
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync(new Tag { Id = model.TagId, UserId = id });

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync((DoorTag)null);

            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsNotFoundObjectResult_WhenTagDoesNotExist()
        {
            int id = 1;
            int invalidTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = invalidTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door
                {
                    UserId = id,
                    Id = mac
                });
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync((Tag)null);

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync((DoorTag)null);

            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsUnauthorizedObjectResult_WhenWrongTagOwner()
        {
            int id = 1;
            int otherUserId = 2;
            int invalidTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = invalidTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door
                {
                    UserId = id,
                    Id = mac
                });
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync(new Tag { Id = model.TagId, UserId = otherUserId });

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync((DoorTag)null);

            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<UnauthorizedObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsUnauthorizedObjectResult_WhenWrongDoorOwner()
        {
            int id = 1;
            int otherUserId = 2;
            int invalidTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = invalidTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door
                {
                    UserId = otherUserId,
                    Id = mac
                });
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync(new Tag { Id = model.TagId, UserId = id });

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync((DoorTag)null);

            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<UnauthorizedObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsBadRequestObjectResult_WhenTagAlreadyAdded()
        {
            int id = 1;
            int validTagId = 5;
            string mac = "macAddress";

            var model = new CreateDoorTagModel()
            {
                TagId = validTagId
            };

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door
                {
                    UserId = id,
                    Id = mac
                });
            var mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository
                 .Setup(m => m.GetAsync(model.TagId))
                 .ReturnsAsync(new Tag
                 {
                     Id = model.TagId,
                     UserId = id
                 });

            var mockDoorTagRepository = new Mock<IDoorTagRepository>();
            mockDoorTagRepository
                .Setup(m => m.GetAsync(mac, model.TagId))
                .ReturnsAsync(new DoorTag
                {
                    TagId = model.TagId,
                    DoorId = mac
                });

            var controller = GetMockedController(id, mockDoorRepository: mockDoorRepository,
                mockTagRepository: mockTagRepository, mockDoorTagRepository: mockDoorTagRepository);
            controller.BindModel(model);

            // act
            var result = await controller.AddTagAsync(mac, model);

            //assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsBadRequestObjectResult_WhenInvalidTag()
        {
            int id = 1;
            int invalidTag = 0;
            string mac = "macAddress";

            var invalidTagModel = new CreateDoorTagModel()
            {
                TagId = invalidTag
            };
            var controller = GetMockedController(id);

            controller.BindModel(invalidTagModel);
            var result = await controller.AddTagAsync(mac, invalidTagModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void AddTagAsync_ReturnsBadRequestObjectResult_WhenInvalidMac()
        {
            int id = 1;
            int validTag = 32;

            var invalidTagModel = new CreateDoorTagModel()
            {
                TagId = validTag
            };
            var controller = GetMockedController(id);

            controller.BindModel(invalidTagModel);
            var result = await controller.AddTagAsync(null, invalidTagModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(invalidTagModel);
            result = await controller.AddTagAsync(string.Empty, invalidTagModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        #endregion

        #region authorize
        [Fact]
        public async void AuthorizeDoorAsync_ReturnsOkObjectResult_WhenValidData()
        {
            var mac = "mac";
            var tag = "tag";
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var openModel = new DoorActionModel() { Tag = tag, Open = true };
            var closeModel = new DoorActionModel() { Tag = tag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(tag, storedHash, storedSalt))
                .Returns(true);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(door);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag> {
                    new Tag() { Salt = storedSalt, Hash = storedHash }
                });
            var mockDoorService = new Mock<IDoorService>();
            mockDoorService
                .Setup(m => m.Act(door.Id, door.Make, door.Model, It.IsAny<bool>()))
                .Returns(true);

            var controller = GetMockedController(mockHasherService: mockHasherService, mockDoorRepository: mockDoorRepository, mockDoorService: mockDoorService);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<OkObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void AuthorizeDoorAsync_ReturnsBadRequestObjectResult_WhenInvalidData()
        {
            var mac = "mac";
            var invalidTag = string.Empty;
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var openModel = new DoorActionModel() { Tag = invalidTag, Open = true };
            var closeModel = new DoorActionModel() { Tag = invalidTag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(invalidTag, storedHash, storedSalt))
                .Returns(true);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(door);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag> {
                    new Tag() { Salt = storedSalt, Hash = storedHash }
                });
            var mockDoorService = new Mock<IDoorService>();
            mockDoorService
                .Setup(m => m.Act(door.Id, door.Make, door.Model, It.IsAny<bool>()))
                .Returns(true);

            var controller = GetMockedController(mockHasherService: mockHasherService,
                mockDoorRepository: mockDoorRepository, mockDoorService: mockDoorService);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void AuthorizeDoorAsync_ReturnsNotFoundObjectResult_WhenTagIsValidButDoorDoesNotExist()
        {
            var mac = "mac";
            var tag = "tag";
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var openModel = new DoorActionModel() { Tag = tag, Open = true };
            var closeModel = new DoorActionModel() { Tag = tag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(tag, storedHash, storedSalt))
                .Returns(true);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync((Door)null);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag>());

            var controller = GetMockedController(mockHasherService: mockHasherService,
                mockDoorRepository: mockDoorRepository);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<NotFoundObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void AuthorizeDoorAsync_ReturnsNotFoundObjectResult_WhenTagIsNotValidButDoorDoesNotExist()
        {
            var mac = "mac";
            var invalidTag = "invalidTag";
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var openModel = new DoorActionModel() { Tag = invalidTag, Open = true };
            var closeModel = new DoorActionModel() { Tag = invalidTag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(invalidTag, storedHash, storedSalt))
                .Returns(false);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync((Door)null);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag>());

            var controller = GetMockedController(mockHasherService: mockHasherService,
                mockDoorRepository: mockDoorRepository);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<NotFoundObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void AuthorizeDoorAsync_ReturnsUnauthorizedObjectResult_WhenWrongTag()
        {
            var mac = "mac";
            var tag = "tag";
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];

            var wrongHash = Enumerable.Repeat((byte)1, 64).ToArray();
            var wrongSalt = Enumerable.Repeat((byte)3, 128).ToArray();

            var openModel = new DoorActionModel() { Tag = tag, Open = true };
            var closeModel = new DoorActionModel() { Tag = tag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(tag, storedHash, storedSalt))
                .Returns(true);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(door);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag> {
                    new Tag() { Salt = wrongSalt, Hash = wrongHash}
                });
            var mockDoorService = new Mock<IDoorService>();
            mockDoorService
                .Setup(m => m.Act(door.Id, door.Make, door.Model, It.IsAny<bool>()))
                .Returns(true);

            var controller = GetMockedController(mockHasherService: mockHasherService, mockDoorRepository: mockDoorRepository, mockDoorService: mockDoorService);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<UnauthorizedObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<UnauthorizedObjectResult>(result);
        }


        [Fact]
        public async void AuthorizeDoorAsync_ReturnsBadRequestObjectResult_WhenDoorActionFails()
        {
            var mac = "mac";
            var tag = "tag";
            var make = "make";
            var model = "model";
            var door = new Door
            {
                Id = mac,
                Make = make,
                Model = model
            };

            var storedHash = new byte[64];
            var storedSalt = new byte[128];
            var openModel = new DoorActionModel() { Tag = tag, Open = true };
            var closeModel = new DoorActionModel() { Tag = tag, Open = false };

            var mockHasherService = new Mock<IHasherService>();
            mockHasherService
                .Setup(m => m.VerifyPasswordHash(tag, storedHash, storedSalt))
                .Returns(true);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(door);
            mockDoorRepository
                .Setup(m => m.GetTagsForDoorAsync(mac))
                .ReturnsAsync(new List<Tag> {
                    new Tag() { Salt = storedSalt, Hash = storedHash }
                });
            var mockDoorService = new Mock<IDoorService>();
            mockDoorService
                .Setup(m => m.Act(door.Id, door.Make, door.Model, It.IsAny<bool>()))
                .Returns(false);

            var controller = GetMockedController(mockHasherService: mockHasherService, mockDoorRepository: mockDoorRepository, mockDoorService: mockDoorService);
            controller.BindModel(openModel);
            var result = await controller.Action(mac, openModel);
            Assert.IsType<BadRequestObjectResult>(result);

            controller.BindModel(openModel);
            result = await controller.Action(mac, openModel);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        #endregion

        #region history
        [Fact]
        public async void GetHistoryAsync_ReturnsOkObjectResult_WhenDoorOwner()
        {
            int id = 1;
            var mac = "mac";

            var expectedList = new List<DoorAccess>() {
                new DoorAccess {
                    Id = 2,
                    CreatedAt = DateTime.UtcNow.AddMilliseconds(-322121),
                    DoorId = mac,
                    Open = true,
                    Success = true,
                    TagId = 3
                }
            };

            var mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync(new User { Id = id, Admin = false });

            var mockDoorAccessRepository = new Mock<IDoorAccessRepository>();
            mockDoorAccessRepository
                .Setup(m => m.GetAllByDoorIdAsync(mac))
                .ReturnsAsync(expectedList);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door { UserId = id, Id = mac });

            var controller = GetMockedController(id,
                mockDoorRepository: mockDoorRepository,
                mockUserRepository: mockUserRepository,
                mockDoorAccessRepository: mockDoorAccessRepository);

            // act
            var result = await controller.GetHistoryAsync(mac);

            //assert
            Assert.IsType<OkObjectResult>(result);
            var actualList = (result as OkObjectResult).Value as List<DoorAccess>;
            Assert.Equal(actualList.Count, expectedList.Count);
            for (int i = 0; i < expectedList.Count; i++)
            {
                AssertCorrectDoorAccess(expectedList[i], actualList[i]);
            }
        }


        [Fact]
        public async void GetHistoryAsync_ReturnsOkObjectResult_WhenAdmin()
        {
            int adminId = 1;
            int ownerId = 2;
            var mac = "mac";

            var expectedList = new List<DoorAccess>() {
                new DoorAccess {
                    Id = 2,
                    CreatedAt = DateTime.UtcNow.AddMilliseconds(-322121),
                    DoorId = mac,
                    Open = true,
                    Success = true,
                    TagId = 3
                }
            };

            var mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(adminId))
                .ReturnsAsync(new User { Id = adminId, Admin = true });

            var mockDoorAccessRepository = new Mock<IDoorAccessRepository>();
            mockDoorAccessRepository
                .Setup(m => m.GetAllByDoorIdAsync(mac))
                .ReturnsAsync(expectedList);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door { UserId = ownerId, Id = mac });

            var controller = GetMockedController(adminId,
                mockDoorRepository: mockDoorRepository,
                mockUserRepository: mockUserRepository,
                mockDoorAccessRepository: mockDoorAccessRepository);

            // act
            var result = await controller.GetHistoryAsync(mac);

            //assert
            Assert.IsType<OkObjectResult>(result);
            var actualList = (result as OkObjectResult).Value as List<DoorAccess>;
            Assert.Equal(actualList.Count, expectedList.Count);
            for (int i = 0; i < expectedList.Count; i++)
            {
                AssertCorrectDoorAccess(expectedList[i], actualList[i]);
            }
        }

        [Fact]
        public async void GetHistoryAsync_ReturnsUnAuthorizedObjetResult_WhenNotOwnerAndNotAdmin()
        {
            int id = 1;
            int ownerId = 2;
            var mac = "mac";

            var expectedList = new List<DoorAccess>() {
                new DoorAccess {
                    Id = 2,
                    CreatedAt = DateTime.UtcNow.AddMilliseconds(-322121),
                    DoorId = mac,
                    Open = true,
                    Success = true,
                    TagId = 3
                }
            };

            var mockUserRepository = new Mock<IUserRepository>();
            mockUserRepository
                .Setup(m => m.GetAsync(id))
                .ReturnsAsync(new User { Id = id, Admin = false });

            var mockDoorAccessRepository = new Mock<IDoorAccessRepository>();
            mockDoorAccessRepository
                .Setup(m => m.GetAllByDoorIdAsync(mac))
                .ReturnsAsync(expectedList);

            var mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door { UserId = ownerId, Id = mac });

            var controller = GetMockedController(id,
                mockDoorRepository: mockDoorRepository,
                mockUserRepository: mockUserRepository,
                mockDoorAccessRepository: mockDoorAccessRepository);

            // act
            var result = await controller.GetHistoryAsync(mac);

            //assert
            Assert.IsType<UnauthorizedObjectResult>(result);

        }

        private void AssertCorrectDoorAccess(DoorAccess expected, DoorAccess actual)
        {
            Assert.Equal(expected.CreatedAt, actual.CreatedAt);
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.TagId, actual.TagId);
            Assert.Equal(expected.DoorId, actual.DoorId);
            Assert.Equal(expected.Success, actual.Success);
            Assert.Equal(expected.Open, actual.Open);
            Assert.Equal(expected.Action, actual.Action);
        }
        #endregion
        #region delete
        [Fact]
        public async void DeleteAsync_ReturnsOkObjectResult_WhenValidData()
        {
            var mac = "mac";
            var userid = 3;

            Mock<IDoorRepository> mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door() { Id = mac, UserId = userid });

            var controller = GetMockedController(userid, mockDoorRepository: mockDoorRepository);

            var result = await controller.DeleteAsync(mac);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void DeleteAsync_ReturnsNotfoundObjectResult_WhenDoorDoesNotExist()
        {
            var mac = "mac";
            var userid = 3;

            Mock<IDoorRepository> mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync((Door)null);

            var controller = GetMockedController(userid, mockDoorRepository: mockDoorRepository);

            var result = await controller.DeleteAsync(mac);
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void DeleteAsync_ReturnsNotAuthorisedObjectResult_WhenTagBelongsToAnotherUser()
        {
            var mac = "mac";
            var userid = 3;
            var ownerId = 2;

            Mock<IDoorRepository> mockDoorRepository = new Mock<IDoorRepository>();
            mockDoorRepository
                .Setup(m => m.GetAsync(mac))
                .ReturnsAsync(new Door() { Id = mac, UserId = ownerId });

            var controller = GetMockedController(userid, mockDoorRepository: mockDoorRepository);

            var result = await controller.DeleteAsync(mac);
            Assert.IsType<UnauthorizedObjectResult>(result);
        }

        #endregion

        private DoorController GetMockedController(int? userId = null,
            Mock<IUserRepository> mockUserRepository = null,
            Mock<ITagRepository> mockTagRepository = null,
            Mock<IDoorRepository> mockDoorRepository = null,
            Mock<IDoorTagRepository> mockDoorTagRepository = null,
            Mock<IDoorAccessRepository> mockDoorAccessRepository = null,
            Mock<IHasherService> mockHasherService = null, Mock<IDoorService> mockDoorService = null)
        {
            return new DoorController(
                (mockUserRepository ?? new Mock<IUserRepository>()).Object,
                (mockDoorRepository ?? new Mock<IDoorRepository>()).Object,
                (mockTagRepository ?? new Mock<ITagRepository>()).Object,
                (mockDoorTagRepository ?? new Mock<IDoorTagRepository>()).Object,
                (mockDoorAccessRepository ?? new Mock<IDoorAccessRepository>()).Object,
                (mockHasherService ?? new Mock<IHasherService>()).Object,
                (mockDoorService ?? new Mock<IDoorService>()).Object,
                _mapper,
                null)
            {
                ControllerContext = GetControllerContext(userId),
            };
        }
    }
}
