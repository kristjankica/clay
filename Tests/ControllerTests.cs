﻿using AutoMapper;
using Contract.Helpers.Mapper;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Tests
{
    public abstract class ControllerTests
    {
        protected IMapper _mapper;
        public ControllerTests()
        {
            var profiles = typeof(UserMapper)
                .Assembly
                .GetTypes()
                .Where(x => typeof(Profile).IsAssignableFrom(x));

            var mappingConfig = new MapperConfiguration(mc =>
            {
                foreach (var profile in profiles)
                {
                    var instance = (Profile)Activator.CreateInstance(profile);
                    mc.AddProfile(instance);
                }

            });
            _mapper = mappingConfig.CreateMapper();
        }
        protected ControllerContext GetControllerContext(int? id = null)
        {
            Mock<ClaimsPrincipal> mockPrincipal = new Mock<ClaimsPrincipal>();
            if (id.HasValue)
            {
                GenericIdentity identity = new GenericIdentity(id.Value.ToString(), "");
                mockPrincipal.Setup(x => x.Identity).Returns(identity);
            }
            else
            {
                mockPrincipal.Setup(x => x.Identity).Returns((GenericIdentity)null);
            }
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(m => m.User).Returns(mockPrincipal.Object);
            return new ControllerContext
            {
                HttpContext = mockHttpContext.Object
            };
        }

        //protected ApplicationDbContext GetDbContext()
        //{
        //    var options = new DbContextOptionsBuilder<ApplicationDbContext>()
        //        .UseInMemoryDatabase(databaseName: "TestDatabase")
        //        .Options;
        //    return new ApplicationDbContext(options);
        //}


    }
}
