﻿using Contract.Services;
using Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services
{
    public class JwtTokenService : IJwtTokenService
    {
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly SigningCredentials _signingCredentials;
        public JwtTokenService(TokenValidationParameters tokenValidationParameters, IOptions<AppSettings> appSettings)
        {
            _tokenValidationParameters = tokenValidationParameters;
            var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
            _signingCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        /// Exceptions:
        ///   T:System.ArgumentNullException:
        ///     userName is null.
        public string GetToken(string userName)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userName),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = _signingCredentials
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public string GetUserName(string token)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var claimsPrincipal = handler.ValidateToken(token, _tokenValidationParameters, out SecurityToken validatedToken);
            return claimsPrincipal.Identity.Name;
        }
    }
}
