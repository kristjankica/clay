﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Contract.Helpers.Mapper;
using Contract.Repositories;
using Contract.Services;
using DataAccess;
using DataAccess.Repositories;
using Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Services;
using IHostingEnvironment = Microsoft.Extensions.Hosting.IHostingEnvironment;
namespace Calories
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
               options =>
               {
                   options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
               }
               );

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            //scan profiles in automapper
            var profiles = typeof(UserMapper).Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));

            var mappingConfig = new MapperConfiguration(mc =>
            {
                foreach (var profile in profiles)
                {
                    var instance = (Profile)Activator.CreateInstance(profile);
                    mc.AddProfile(instance);
                }

            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);


            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);



            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true
            };
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = async context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IUserRepository>();
                        var userId = int.Parse(context.Principal.Identity.Name);
                        if (!await userService.ExistsAsync(userId))
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("User has been removed");
                        }

                    }
                    };
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = validationParameters;
                });

            services.AddMvc();

            // configure DI for application services
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDoorRepository, DoorRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IDoorTagRepository, DoorTagRepository>();
            services.AddScoped<IDoorService, DoorService>();
            services.AddScoped<IDoorAccessRepository, DoorAccessRepository>();
            services.AddSingleton<IEmailValidatorService, EmailValidatorService>();
            services.AddSingleton(validationParameters);
            services.AddSingleton<IJwtTokenService, JwtTokenService>();
            services.AddSingleton<IHasherService, HasherService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseMvc();
        }
    }
}
