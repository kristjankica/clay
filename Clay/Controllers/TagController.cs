﻿using System;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Contract.Data;
using Microsoft.Extensions.Logging;
using Contract.Repositories;
using Contract.Services;
using System.Threading.Tasks;
using Contract.Models.Tags;

namespace Controllers
{
    [ApiController]
    [Authorize]
    [Route("tags")]
    [Produces("application/json")]
    public class TagController : BaseController
    {
        private readonly ITagRepository _tagRepository;
        private readonly IMapper _mapper;
        private readonly IHasherService _hasherService;

        public TagController(
            IUserRepository userRepository,
            ITagRepository tagRepository,
            IMapper mapper,
            IHasherService hasherService,
            ILogger<TagController> logger) : base(userRepository, logger)
        {
            _tagRepository = tagRepository;
            _hasherService = hasherService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync()
        {
            try
            {
                var generatedTag = Guid.NewGuid().ToString();
                _hasherService.CreatePasswordHash(generatedTag, out byte[] hash, out byte[] salt);

                var tag = new Tag()
                {
                    Hash = hash,
                    Salt = salt,
                    UserId = MyId.Value
                };

                await _tagRepository.AddAsync(tag);
                var model = _mapper.Map<TagModel>(tag);
                model.Tag = generatedTag;
                return Ok(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute]int id)
        {
            try
            {
                var tag = await _tagRepository.GetAsync(id);
                if (tag == null)
                    return NotFound(resourceNotFoundMessage);
                if (tag.UserId != MyId.Value)
                    return Unauthorized(notAuthorizedMessage);
                await _tagRepository.DeleteAsync(tag);
                return Ok(successMessage);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}