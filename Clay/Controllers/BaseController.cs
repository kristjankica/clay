﻿using Contract.Data;
using Contract.Repositories;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Controllers
{
    [ApiController]
    [Authorize]
    public class BaseController : ControllerBase
    {
        protected readonly IUserRepository _userRepository;
        protected readonly ILogger _logger;
        protected const string resourceNotFoundMessage = "A required resource was not found";
        protected const string resourceDeletedMessage = "A required resource has been deleted";
        protected const string notAuthorizedMessage = "You are not authorized to this resource";
        protected const string resourceAlreadyExistsMessage = "A resource you are trying to create has already been created";

        protected const string successMessage = "The operation was successfull";

        public BaseController(IUserRepository userRepository, ILogger logger)
        {
            _userRepository = userRepository;
            _logger = logger;

        }

        protected int? MyId
        {
            get
            {
                if (User.Identity?.Name != null && int.TryParse(User.Identity.Name, out int id))
                    return id;
                return null;
            }
        }

        protected Task<User> MeAsync => _userRepository.GetAsync(MyId.Value);

        public override BadRequestObjectResult BadRequest(object error = null)
        {
            _logger?.LogError(error?.ToString());
            return base.BadRequest(error);
        }

        protected BadRequestObjectResult BadRequest(Exception exception)
        {
            _logger?.LogError(exception.Message);
            return base.BadRequest("An unfortunate error occurred!");
        }
    }


}
