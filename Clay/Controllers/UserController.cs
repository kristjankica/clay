﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Helpers;
using Contract.Models.Users;
using Contract.Data;
using Microsoft.Extensions.Logging;
using Contract.Repositories;
using Contract.Services;
using System.Threading.Tasks;

namespace Controllers
{
    [ApiController]
    [Authorize]
    [Route("users")]
    [Produces("application/json")]
    public class UserController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IJwtTokenService _jwtTokenService;
        private readonly IHasherService _hasherService;
        private readonly IEmailValidatorService _emailValidatorService;

        public UserController(
            IUserRepository userRepository,
            IMapper mapper,
            IJwtTokenService jwtTokenService,
            IEmailValidatorService emailValidatorService,
            IHasherService hasherService,
            ILogger<UserController> logger) : base(userRepository, logger)
        {
            _jwtTokenService = jwtTokenService;
            _emailValidatorService = emailValidatorService;
            _hasherService = hasherService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> RegisterAsync([FromBody]RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            // map model to entity
            var user = _mapper.Map<User>(model);

            try
            {

                if (!_emailValidatorService.Validate(user.Email))
                    return BadRequest("This email is not in the correct format");
                if (await _userRepository.GetByEmailAsync(user.Email) != null)
                    return BadRequest("Email \"" + user.Email + "\" is already taken");

                _hasherService.CreatePasswordHash(model.Password, out byte[] passwordHash, out byte[] passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                await _userRepository.AddAsync(user);
                return Ok(successMessage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody]AuthenticateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            User user;
            try
            {
                user = await _userRepository.GetByEmailAsync(model.Email);

                if (user == null)
                    return NotFound(resourceNotFoundMessage);

                // check if password is correct
                if (!_hasherService.VerifyPasswordHash(model.Password, user.PasswordHash, user.PasswordSalt))
                {
                    return Unauthorized(notAuthorizedMessage);
                }
                // return basic user info and authentication token
                return Ok(new
                {
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Token = _jwtTokenService.GetToken(user.Id.ToString())
                });

            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }


        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var users = await _userRepository.GetAllAsync();
            var model = _mapper.Map<IEnumerable<UserModel>>(users);
            return Ok(model);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync([FromRoute]int id)
        {
            try
            {
                if (id != MyId.Value)
                    return Unauthorized();

                var user = await _userRepository.GetAsync(id);
                if (user == null)
                    return NotFound(resourceNotFoundMessage);
                var model = _mapper.Map<UserModel>(user);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }


        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync([FromRoute]int id, [FromBody]UpdateUserModel model)
        {
            try
            {
                if (id != MyId.Value)
                    return Unauthorized();

                // update user 
                var user = await _userRepository.GetAsync(id);

                if (user == null)
                    return NotFound(resourceNotFoundMessage);

                // update user properties if provided
                if (!string.IsNullOrWhiteSpace(model.FirstName))
                    user.FirstName = model.FirstName;

                if (!string.IsNullOrWhiteSpace(model.LastName))
                    user.LastName = model.LastName;

                // update password if provided
                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    byte[] passwordHash, passwordSalt;
                    _hasherService.CreatePasswordHash(model.Password, out passwordHash, out passwordSalt);

                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;
                }

                await _userRepository.UpdateAsync(user);
                return Ok(successMessage);
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute]int id)
        {
            if (id != MyId.Value)
                return Unauthorized(notAuthorizedMessage);
            await _userRepository.DeleteAsync(id);
            return Ok(successMessage);
        }


        private async Task<User> AuthenticateAsync(string email, string password)
        {
            var user = await _userRepository.GetByEmailAsync(email);

            if (user == null)
                return null;

            // check if password is correct
            if (!_hasherService.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            // authentication successful
            return user;
        }


    }
}