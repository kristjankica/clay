﻿using AutoMapper;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Contract.Data;
using Contract.Repositories;
using Contract.Models.DoorsTags;
using Contract.Models.Doors;
using Contract.Models.Tags;
using System.Linq;
using Contract.Services;

namespace Controllers
{
    [ApiController]
    [Authorize]
    [Route("doors")]
    [Produces("application/json")]
    public class DoorController : BaseController
    {
        private readonly IDoorRepository _doorRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IDoorTagRepository _doorTagRepository;
        private readonly IDoorAccessRepository _doorAccessRepository;
        private readonly IHasherService _hasherService;
        private readonly IDoorService _doorService;
        private readonly IMapper _mapper;

        public DoorController(
            IUserRepository userRepository,
            IDoorRepository doorRepository,
            ITagRepository tagRepository,
            IDoorTagRepository doorTagRepository,
            IDoorAccessRepository doorAccessRepository,
            IHasherService hasherService,
            IDoorService doorService,
            IMapper mapper,
            ILogger<DoorController> logger) : base(userRepository, logger)
        {
            _doorRepository = doorRepository;
            _tagRepository = tagRepository;
            _doorTagRepository = doorTagRepository;
            _doorAccessRepository = doorAccessRepository;
            _hasherService = hasherService;
            _doorService = doorService;
            _mapper = mapper;
        }

        [HttpGet("{id}/history")]
        public async Task<IActionResult> GetHistoryAsync([FromRoute] string id)
        {
            try
            {
                var door = await _doorRepository.GetAsync(id);
                var authorized = door?.UserId == MyId || (await MeAsync).Admin;
                if (!authorized)
                {
                    return Unauthorized(notAuthorizedMessage);
                }

                var history = await _doorAccessRepository.GetAllByDoorIdAsync(id);
                return Ok(history);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody]CreateDoorModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            try
            {
                if (await _doorRepository.ExistsAsync(model.Mac))
                {
                    return BadRequest(resourceAlreadyExistsMessage);
                }

                // map model to entity
                var door = _mapper.Map<Door>(model);

                door.UserId = MyId.Value;
                await _doorRepository.AddAsync(door);
                return Ok(_mapper.Map<DoorModel>(door));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost("{id}/tags")]
        public async Task<IActionResult> AddTagAsync([FromRoute]string id, [FromBody]CreateDoorTagModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("Door mac is not valid");
            }
            try
            {
                var door = await _doorRepository.GetAsync(id);
                var tag = await _tagRepository.GetAsync(model.TagId);

                if (door == null || tag == null)
                    return NotFound(resourceNotFoundMessage);

                int myId = MyId.Value;
                if (door.UserId != myId || tag.UserId != myId)
                {
                    return Unauthorized(notAuthorizedMessage);
                }

                var doorTag = await _doorTagRepository.GetAsync(id, model.TagId);
                if (doorTag != null)
                    return BadRequest(resourceAlreadyExistsMessage);
                // map model to entity
                doorTag = _mapper.Map<DoorTag>(model);
                doorTag.DoorId = id;

                await _doorTagRepository.AddAsync(doorTag);
                return Ok(successMessage);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [AllowAnonymous]
        [HttpPost("{id}/action")]
        public async Task<IActionResult> Action([FromRoute]string id, [FromBody]DoorActionModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            try
            {
                var door = await _doorRepository.GetAsync(id);

                if (door == null)
                    return NotFound(resourceNotFoundMessage);

                var tags = await _doorRepository.GetTagsForDoorAsync(id);

                var authorizedTag = tags
                    .FirstOrDefault(t => _hasherService.VerifyPasswordHash(model.Tag, t.Hash, t.Salt));

                var doorAccess = new DoorAccess() { DoorId = id, Open = model.Open };
                if (authorizedTag == null)
                {
                    await _doorAccessRepository.AddAsync(doorAccess);
                    return Unauthorized(notAuthorizedMessage);
                }
                else
                {
                    doorAccess.TagId = authorizedTag.Id;
                    if (_doorService.Act(door.Id, door.Make, door.Model, model.Open))
                    {
                        doorAccess.Success = true;
                        await _doorAccessRepository.AddAsync(doorAccess);
                        return Ok(successMessage);
                    }
                    await _doorAccessRepository.AddAsync(doorAccess);
                    return BadRequest("Door action failed");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute]string id)
        {
            try
            {
                var door = await _doorRepository.GetAsync(id);
                if (door == null)
                    return NotFound(resourceNotFoundMessage);
                if (door.UserId != MyId.Value)
                    return Unauthorized(notAuthorizedMessage);
                await _doorRepository.DeleteAsync(door);
                return Ok(successMessage);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}